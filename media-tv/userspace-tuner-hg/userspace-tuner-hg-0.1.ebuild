# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils linux-mod mercurial

DESCRIPTION="Live development version of the userspace-tuner project"
HOMEPAGE="http://mcentral.de/wiki/index.php/Userspace_tuner"
SRC_URI=""
EHG_REPO_URI="http://mcentral.de/hg/~mrec/userspace-tuner"
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE=""
DEPEND=""
RDEPEND="sys-fs/udev"
S="${WORKDIR}/userspace-tuner"

pkg_setup() {

	MODULE_NAMES="media-stub(kernel/drivers/media/userspace:${S}/kernel)"
	BUILD_TARGETS="all"

	linux-mod_pkg_setup

	BUILD_PARAMS="KDIR=${KV_DIR}"
}

src_unpack() {

	mercurial_src_unpack
	cd "${S}"
	EPATCH_SUFFIX="patch"
	epatch "${FILESDIR}"
}

src_compile() {

	cd "${S}/kernel"
	linux-mod_src_compile || die "failed to build kernel module"

	cd "${S}/userspace"
	emake INCDIR="${KV_DIR}/include" || die "failed to build tuner daemon"
}

src_install() {

	linux-mod_src_install || die "failed to install kernel module"

	cd "${S}/userspace"
	make DESTDIR="${D}" install || die "failed to install tuner daemon"

	newinitd "${S}"/init.d/gentoo-tuner-daemon tuner-daemon
	newconfd "${S}"/conf.d/tuner-daemon tuner-daemon

	insinto /etc/udev/rules.d
	doins "${S}"/udev/80-tuner.rules

	cd "${S}"
	dodoc README*
}

pkg_postinst() {

	linux-mod_pkg_postinst

	elog ""
	elog "Currently supported tuners:"
	elog "- Xceive XC2028, XC3028"
	elog ""
	elog "The /etc/init.d/tuner-daemon script does NOT have to be"
	elog "added to any of the runlevels via rc-update, it will"
	elog "be started by udev on demand."
	elog ""
	ewarn "WARNING: This work is highly experimental at the moment!"
	ewarn ""
	ewarn "Your TV-card's in-kernel driver has to be modified"
	ewarn "to properly interface with this software."
	ewarn ""
	ewarn "For more information, see the project's homepage at:"
	ewarn "${HOMEPAGE}"
	ewarn ""
}
